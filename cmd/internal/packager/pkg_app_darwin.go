//----------------------------------------
//
// Copyright © yanghy. All Rights Reserved.
//
// Licensed under Apache License Version 2.0, January 2004
//
// https://www.apache.org/licenses/LICENSE-2.0
//
//----------------------------------------

//go:build darwin
// +build darwin

package packager

import (
	"github.com/energye/energy/v2/cmd/internal/project"
)

func GeneraInstaller(projectData *project.Project) error {
	return nil
}
